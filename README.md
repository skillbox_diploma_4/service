## Runtime
Приложение отвечает по 3 эндпоинтам:  
* /health - 200 ok
* /metrics - в формате метрик для prometheus, включая счётчик запросов в основной эндпоинт `skillbox_http_requests_total`
* / - основной эндпоинт, возвращающий часть запроса и генерирующий строчку лога.

## Документация
Документация, по принятым инфраструктурным решениям находится в репозитории service, в каталоге doc/adr

## Порядок создания сервиса:
1. Клонировать репозиторий infra и развернуть необходимую инфраструктуру
   https://gitlab.com/skillbox_diploma/infra.git

2. Клонировать репозиторий service (репозиторий имеет 2 ветки: main и uat) (должен распологаться в том же каталоге что и репозиторий infra)
   https://gitlab.com/skillbox_diploma/service.git

3. Экспортировать данные для доступа к ресурсам AWS (AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY). 
   ```shell
   export AWS_ACCESS_KEY_ID=[указать значение]
   export AWS_SECRET_ACCESS_KEY=[указать значение]
   ``` 
4. Изменить в плане terraform используемые доменные имена для ресурсов (система мониторинга, веб-приложение) на ваши.

5. Перейти в каталог тестового или продуктивного окружения terraform/environments/ репозитория infra, прописать имя своего ssh ключа в файле main.tf и запустить план для разворачивания инфраструктуры командой
   - для тестового окружения:
   ```shell
   terraform apply -var-file="test.tfvars" --auto-approve
   ```
   - для продуктивного окружения:
   ```shell
   terraform apply -var-file="prod.tfvars" --auto-approve
   ```
6. Перейти в каталог ansible репозитория infra и запустить плэйбуки для настройки защиты сокета docker и регистрации необходимых раннеров на Gitlab.com (прописать токены для регистрации раннера на сервере gitlab.com в переменную reg_token плэйбука 01_register_gitlab_runner.yml)
   Перед запуском плейбука выполнить настройку окружения:
   ```shell
   export ANSIBLE_REMOTE_USER=ubuntu ANSIBLE_INVENTORY=./ansible_plugins ANSIBLE_HOST_KEY_CHECKING=False
   ```
   
   Запустить плейбук для настройки защиты сокета docker (в экстра переменных передаем имя сервера, окружение и доменное имя, для которого будут регистрировать самоподписанные сертификаты)
   - для тестового окружения:
   ```shell
   ansible-playbook -e "passed_in_hosts=tag_Name_Gitlab_Runner_Server:&tag_Environment_test docker_host_domain_name=test.docker.vmsap.tk" 01_protect_docker_socket.yml
   ```
   - для продуктивного окружения:
   ```shell
   ansible-playbook -e "passed_in_hosts=tag_Name_Gitlab_Runner_Server:&tag_Environment_prod docker_host_domain_name=docker.vmsap.tk" 01_protect_docker_socket.yml
   ```

   Запустить плейбук для регистрации раннеров (в экстра переменных передаем имя сервера, окружение и доменное имя, которое будут использвать клиенты для подключение к docker)
   - для тестового окружения:
   ```shell
   ansible-playbook -e "passed_in_hosts=tag_Name_Gitlab_Runner_Server:&tag_Environment_test srv_env=test docker_host_domain_name=test.docker.vmsap.tk" 02_register_gitlab_runner_secure.yml --ask-vault-pass
   ```
   - для продуктивного окружения:
   ```shell
   ansible-playbook -e "passed_in_hosts=tag_Name_Gitlab_Runner_Server:&tag_Environment_prod srv_env=prod docker_host_domain_name=docker.vmsap.tk" 02_register_gitlab_runner_secure.yml --ask-vault-pass
   ```

7. Тестирование кода приложения на golang осуществляется при помощи линтера golangci-lint.
   Тестируется код расположенный в папке - ./cmd/server.
   Изменить каталог, добавить или удалить тесты можно в строке запуска линтера в файле .gitlab-ci.yml. Стэйдж - test_syntax.
   
8. Push из репозиторя service в ветку main запускает пайплан для продуктивного окружения. В ветку uat - для тестового.

9. Пайплайн тестирует код, собирает образ контейнера и деплоит его на целевой сервер (для соответствующего окружения).
   Необходимо в разделе variables файла .gitlab-ci.yml задать переменные project_version и project_name.

10. Собранный образ контейнера пушится в репозиторий gitlab. Стэйдж - build_image.

11. Деплой осуществляется запуском ansible роли manage_docker_containers из подкаталога ansible. Стэйдж - deploy_image.
   Переменные в роль передаются при выполнении стэйджа deploy_image.

