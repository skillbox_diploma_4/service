## Статус:

Принято

## Контекст решения:

Лучшие практики рекомендуют использовать для https для подключения к сервису docker

## Решение:

Принято решение добавить плейбук, генерирующий самоподписанные сертификаты для защиты сокета docker
infra/ansible/01_protect_docker_socket.yml
 
 ## Последствия
 
Работа с docker стала более защищенной.
Требуется запуск дополнительного ansible playbook для настройки.